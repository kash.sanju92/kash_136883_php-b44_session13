<?php

// __callStatic() method call hobe jkhn class a Kono Static method nai but ta call kora holo.. ai khetre call hoy

class Person   // method class er vitor declare hoy tai class er moddhe method ta boshalam

{
    public $name;

    public static $msg; // static property er lhetre access hoy

    /*Static method tokhny use korbo jokhn frequntly  hoy orthat class er jono method ke bar bar call kora lage
    obj call na koreo amra direct static method call korte pari. */

   public static function doFrequently() // static method er khetreo access hoy
   {
       echo "I am doing it frequently <br>";
   }

    public static function __callStatic($name, $arguments)
    {
        echo "I am inside call static method ".__METHOD__. "<br>";
        echo " wrong static name = $name<br>"; // vul method er name ta hold korbe
        echo "<pre>";
        print_r($arguments); //arguments gulo print korabe
        echo "</pre>";
    }

    public function __construct()
    {
        echo "I am inside the :". __METHOD__ . "<br>" ;
    }

    public function __destruct()
    {
        echo "I am dying. I am inside the :". __METHOD__ . "<br>" ;
    }

}

Person::doFrequentlyiiiiiii("jama", "kapor"); // kono obj er charai call korlam. ai vabe call kora hoy. Static call holo


Person::$msg = "I am inside message <br>";

echo Person::$msg; // static jokhn property er khetre access hoche


$obj = new Person();

$obj->doFrequently(); // static akta function call korlam. er jonno obj creation lagbe

//kono object ke class er under a nile shob method call hobe na magic methods chara.
