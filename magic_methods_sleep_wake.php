<?php

// sleep kaj kore serialize er sathe... unserialize er sathe kaj kore wake up


class Person

{
    public $name = "Kashfia", $address = "Ctg", $dob = "18th May, 1992";


    //sleep er kaj holo property jegulo amra serialize korte chai ta specific kore bole dewa. eti array akare return korbe

    public function doSomething()
    {
        echo "I am inside the do something" . "<br>" ;
    }

    public function __sleep()
    {
        return array("name", "dob"); // jeta jeta serialize korte chai just shetai hobe
    }

    public function __wakeup()   //wake up ta use korbo jokhn unserialize korbo tokhn. ekhetre jeta serialize kora chilou na shetao show hobe
    {
        echo "<br> I am inside wakeup <br>";
        $this->doSomething();
    }


}

$obj = new Person();

$myVar = serialize($obj);

echo "<pre>";

var_dump($myVar);

echo "</pre>";

var_dump (unserialize($myVar));

