<?php


// __call methods

class Person   // method class er vitor declare hoy tai class er moddhe method ta boshalam

{
    public $name;

    public function doSomething()
    {
        echo "I am inside the do something" . "<br>" ;
    }

    public function __construct()
    {
        echo "I am inside the :". __METHOD__ . "<br>" ;
    }

    public function __destruct()
    {
        echo "I am dying. I am inside the :". __METHOD__ . "<br>" ;
    }

    public function __call($name, $arguments) // ai method ta call hobe jokhn method name vul hoy
    {
        echo "I am inside call method ".__METHOD__. "<br>";
        echo " name = $name<br>"; // vul method er name ta hold korbe
        echo "<pre>";
        print_r($arguments); //arguments gulo print korabe
        echo "</pre>";
    }

}

$obj = new Person();

$obj->doSomething();

$obj->habijabi("para_habi1", "para_jabi1");  // ultapalta kono method call korlam jar vitore 2 ta perameter dilam