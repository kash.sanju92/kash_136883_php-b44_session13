<?php

// __tostring : object ke string akare read er jonno use hoy

class Person

{
    public $name;

    public function __toString()  //object ke string akare use korte gelei eta call hobe
    {
        return "Are you crazy?? I am just an Object<br>";
    }

    public function __invoke()  // object ke function hishebe call korte chaile eta call hobe
    {
        echo "<br>I am not a function man. I am an object. <br>";
    }

}

$obj = new Person();

echo $obj; // object ke string akare print korte chaile __toString call hocche.

$obj();

